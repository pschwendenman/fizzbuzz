// fizz_buzz.c

#include "fizz_buzz.h"

#include <stdio.h>
#include <string.h>

char word[9];

char* fizzbuzz(int num) {
  if ((num % 3 == 0) && (num % 5 == 0)) {
    strcpy(word, "fizzbuzz");
      } else if (num % 3 == 0) {
    strcpy(word, "fizz");
  } else if (num % 5 == 0) {
    strcpy(word, "buzz");
  } else {
    sprintf(word, "%d", num);
  }

  return word;
}

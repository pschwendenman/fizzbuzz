// fizz_buzz_test.c

#include "fizz_buzz.h"

#include <assert.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#define test_func(str, func) { fprintf(stderr, "%s... ", str); func(); fprintf(stderr, "pass\n");}

#define test_fizzbuzz_returns_number(num, output) { assert(strcmp(fizzbuzz(num), output) == 0); }

static void test_one_returns_one() {
  test_fizzbuzz_returns_number(1, "1");
}

static void test_two_returns_two() {
  test_fizzbuzz_returns_number(2, "2");
}

static void test_three_returns_foo() {
  test_fizzbuzz_returns_number(3, "fizz");
}

static void test_six_returns_foo() {
  test_fizzbuzz_returns_number(6, "fizz");
}

static void test_five_returns_buzz() {
  test_fizzbuzz_returns_number(5, "buzz");
}

static void test_fifteen_returns_fizzbuzz() {
  test_fizzbuzz_returns_number(15, "fizzbuzz");
}

int main() {
  test_func("test one returns one", test_one_returns_one);
  test_func("test two returns two", test_two_returns_two);
  test_func("test three returns fizz", test_three_returns_foo);
  test_func("test six returns fizz", test_six_returns_foo);
  test_func("test five returns buzz", test_five_returns_buzz);
  test_func("test fifteen returns fizzbuzz", test_fifteen_returns_fizzbuzz);
}

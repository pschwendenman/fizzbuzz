package com.fizzbuzz;

import static org.junit.Assert.*;
import org.junit.*;

public class FizzbuzzTest {

    @Test public void test_one_returns_one() {
        Fizzbuzz fb = new Fizzbuzz();
        String out = fb.fizzbuzz(1);
        assertEquals(out, "1");
    }

    @Test public void test_two_returns_two() {
        Fizzbuzz fb = new Fizzbuzz();
        String out = fb.fizzbuzz(2);
        assertEquals(out, "2");
    }

    @Test public void test_three_returns_fizz() {
        Fizzbuzz fb = new Fizzbuzz();
        String out = fb.fizzbuzz(3);
        assertEquals(out, "fizz");
    }

    @Test public void test_nine_returns_fizz() {
        Fizzbuzz fb = new Fizzbuzz();
        String out = fb.fizzbuzz(9);
        assertEquals(out, "fizz");
    }

    @Test public void test_five_returns_buzz() {
        Fizzbuzz fb = new Fizzbuzz();
        String out = fb.fizzbuzz(5);
        assertEquals(out, "buzz");
    }

    @Test public void test_ten_returns_buzz() {
        Fizzbuzz fb = new Fizzbuzz();
        String out = fb.fizzbuzz(10);
        assertEquals(out, "buzz");
    }

    @Test public void test_fifteen_returns_fizzbuzz() {
        Fizzbuzz fb = new Fizzbuzz();
        String out = fb.fizzbuzz(15);
        assertEquals(out, "fizzbuzz");
    }

}

package com.fizzbuzz;

public class Fizzbuzz {
  public String fizzbuzz(int num) {
    String output;
    if (num % 15 == 0) {
      output = "fizzbuzz";
    } else if (num % 3 == 0) {
      output = "fizz";
    } else if (num % 5 == 0) {
      output = "buzz";
    } else {
      output = Integer.toString(num);
    }
    return output;
  }
}

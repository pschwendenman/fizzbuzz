'''
Unit tests for fizzbuzz
'''
import unittest
from fizzbuzz import fizz_buzz, fizz_buzz_ugly


class TestFizzBuzz(unittest.TestCase):
    '''
    Tests for fizzbuzz
    '''

    def test_fizzbuzz_given_one_returns_one(self):
        self.assertEqual(fizz_buzz(1), "1")

    def test_fizzbuzz_given_two_returns_two(self):
        self.assertEqual(fizz_buzz(2), "2")

    def test_fizzbuzz_given_three_returns_fizz(self):
        self.assertEqual(fizz_buzz(3), "fizz")

    def test_fizzbuzz_given_six_returns_fizz(self):
        self.assertEqual(fizz_buzz(6), "fizz")

    def test_fizzbuzz_given_five_returns_buzz(self):
        self.assertEqual(fizz_buzz(5), "buzz")

    def test_fizzbuzz_given_fifteen_returns_fizzbuzz(self):
        self.assertEqual(fizz_buzz(15), "fizzbuzz")

class TestUglyFizzBuzz(unittest.TestCase):
    '''
    Tests for ugly fizzbuzz
    '''
    def test_fizzbuzz_given_one_returns_one(self):
        self.assertEqual(fizz_buzz_ugly(1), "1")

    def test_fizzbuzz_given_two_returns_two(self):
        self.assertEqual(fizz_buzz_ugly(2), "2")

    def test_fizzbuzz_given_three_returns_fizz(self):
        self.assertEqual(fizz_buzz_ugly(3), "fizz")

    def test_fizzbuzz_given_six_returns_fizz(self):
        self.assertEqual(fizz_buzz_ugly(6), "fizz")

    def test_fizzbuzz_given_five_returns_buzz(self):
        self.assertEqual(fizz_buzz_ugly(5), "buzz")

    def test_fizzbuzz_given_fifteen_returns_fizzbuzz(self):
        self.assertEqual(fizz_buzz_ugly(15), "fizzbuzz")

if __name__ == '__main__':
    unittest.main()

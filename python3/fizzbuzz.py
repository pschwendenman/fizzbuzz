'''
Fizz buzz
'''


def is_factor_of(factor, num):
    '''
    Test if factor divides into num
    '''
    return num % factor == 0


def fizz_buzz(num):
    '''
    Classic fizzbuzz
    '''
    if is_factor_of(3 * 5, num):
        return "fizzbuzz"
    elif is_factor_of(3, num):
        return "fizz"
    elif is_factor_of(5, num):
        return "buzz"
    return str(num)

def fizz_buzz_ugly(num):
    '''
    A less "pythonic" implementation
    '''
    return 'fizz' * is_factor_of(3, num) + 'buzz' * is_factor_of(5, num) or str(num)

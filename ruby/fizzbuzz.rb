def fizzbuzz(num)
  output = num % 3 == 0 ? "fizz" : ""
  output += "buzz" if num % 5 == 0
  output.empty? ? num.to_s : output
end

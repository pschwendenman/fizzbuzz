# FizzBuzz in Ruby

## Setup environment

Install bundler and dependencies

```
gem install bundler
bundle install
```

## Running the tests

```
rspec
```

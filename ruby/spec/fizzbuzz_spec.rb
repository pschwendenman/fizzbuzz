require 'rspec'
require './fizzbuzz'

describe "fizz_buzz" do
  it 'should return fizzbuzz for input 0' do
    expect(fizzbuzz(0)).to eq('fizzbuzz')
  end
  it 'should return 1 for input 1' do
    expect(fizzbuzz(1)).to eq('1')
  end
  it 'should return 2 for input 2' do
    expect(fizzbuzz(2)).to eq('2')
  end
  it 'should return fizz for input 3' do
    expect(fizzbuzz(3)).to eq('fizz')
  end
  it 'should return fizz for multiple of 3: 6' do
    expect(fizzbuzz(6)).to eq('fizz')
  end
  it 'should return buzz for input 5' do
    expect(fizzbuzz(5)).to eq('buzz')
  end
  it 'should return fizzbuzz for input 15' do
    expect(fizzbuzz(15)).to eq('fizzbuzz')
  end
end

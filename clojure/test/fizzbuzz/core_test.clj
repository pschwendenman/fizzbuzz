(ns fizzbuzz.core-test
  (:require [clojure.test :refer :all]
            [fizzbuzz.core :refer :all]))

(deftest test-one-begets-one
  (testing "Input of one is one"
    (is (= (fizzbuzz 1) "1"))))

(deftest test-two-begets-two
  (testing "Input of two returns two"
    (is (= (fizzbuzz 2) "2"))))

(deftest test-three-begets-fizz
  (testing "Input of three returns fizz"
    (is (= (fizzbuzz 3) "fizz"))))

(deftest test-six-begets-fizz
  (testing "Input of three returns fizz"
    (is (= (fizzbuzz 6) "fizz"))))

(deftest test-five-begets-buzz
  (testing "Input of five returns buzz"
    (is (= (fizzbuzz 5) "buzz"))))

(deftest test-fifteen-begets-fizzbuzz
  (testing "Input of fifteen returns fizzbuzz"
    (is (= (fizzbuzz 15) "fizzbuzz"))))

(ns fizzbuzz.core
  (:gen-class))

(require '[clojure.core.match :refer [match]])

(defn fizzbuzz [num]
  (match [(mod num 3) (mod num 5)]
    [0 0] "fizzbuzz"
    [0 _] "fizz"
    [_ 0] "buzz"
    :else (str num)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (doseq [x (range 1 16)]
    (println (fizzbuzz x))))

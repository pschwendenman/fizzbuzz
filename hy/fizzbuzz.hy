(defn multiple? [dividend divisor]
  (= (% dividend divisor) 0))

(defn fizzbuzz [num]
  (cond
    [(and (multiple? num 3) (multiple? num 5)) "fizzbuzz"]
    [(multiple? num 3) "fizz"]
    [(multiple? num 5) "buzz"]
    [true (str num)]))

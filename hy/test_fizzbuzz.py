import hy
import unittest

from fizzbuzz import fizzbuzz

class TestFizzBuzz(unittest.TestCase):
    def test_fizzbuzz_given_one_returns_one(self):
        self.assertEqual(fizzbuzz(1), "1")

    def test_fizzbuzz_given_two_returns_two(self):
        self.assertEqual(fizzbuzz(2), "2")

    def test_fizzbuzz_given_three_returns_fizz(self):
        self.assertEqual(fizzbuzz(3), "fizz")

    def test_fizzbuzz_given_six_returns_fizz(self):
        self.assertEqual(fizzbuzz(6), "fizz")

    def test_fizzbuzz_given_five_returns_buzz(self):
        self.assertEqual(fizzbuzz(5), "buzz")

    def test_fizzbuzz_given_fifteen_returns_fizzbuzz(self):
        self.assertEqual(fizzbuzz(15), "fizzbuzz")


if __name__ == '__main__':
    unittest.main()

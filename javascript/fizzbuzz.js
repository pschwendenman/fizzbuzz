module.exports = {
    fizzbuzz: fizzbuzz
};

function is_factor_of(num, factor, string) {
  if (!(num % factor == 0)) {
    string = "";
  }
  return string
}

function fizzbuzz(num) {
  return "".concat(is_factor_of(num, 3, "fizz"),
                   is_factor_of(num, 5, "buzz")) || num.toString();
}

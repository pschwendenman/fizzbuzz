var Fizzbuzz = require('../');
var assert = require('assert');

describe('fizzbuzz', function () {
  it('it should return one with input one', function () {
    assert.equal("1", Fizzbuzz.fizzbuzz(1));
  });
  it('it should return two with input two', function () {
    assert.equal("2", Fizzbuzz.fizzbuzz(2));
  });
  it('it should return fizz with input three', function () {
    assert.equal("fizz", Fizzbuzz.fizzbuzz(3));
  });
  it('it should return fizz with input six', function () {
    assert.equal("fizz", Fizzbuzz.fizzbuzz(6));
  });
  it('it should return buzz with input five', function () {
    assert.equal("buzz", Fizzbuzz.fizzbuzz(5));
  });
  it('it should return fizz with input fifteen', function () {
    assert.equal("fizzbuzz", Fizzbuzz.fizzbuzz(15));
  });
});

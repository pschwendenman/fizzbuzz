Fizzbuzz in Rust
==================

Installation
-------------

To install on OSX, use brew::

    brew install rust

To check that rust is installed::

    rustc --version

Setup new project
------------------

::

    cargo new fizzbuzz --bin

Build
-------

::

    cargo build

Test
------

::

    cargo test

Run
-----

::

    cargo run

fn main() {
    for x in 1..16 {
        println!("{}", fizzbuzz(x))
    }
}

fn fizzbuzz(num: i64) -> String {
    match (num % 3, num % 5) {
        (0, 0) => "fizzbuzz".to_string(),
        (0, _) => "fizz".to_string(),
        (_, 0) => "buzz".to_string(),
        _ => num.to_string()
    }
}

#[test]
fn one_returns_one() {
    assert_eq!(fizzbuzz(1), "1".to_string())
}

#[test]
fn two_returns_two() {
    assert_eq!(fizzbuzz(2), "2".to_string())
}

#[test]
fn three_returns_fizz() {
    assert_eq!(fizzbuzz(3), "fizz".to_string())
}

#[test]
fn six_returns_fizz() {
    assert_eq!(fizzbuzz(6), "fizz".to_string())
}

#[test]
fn five_returns_buzz() {
    assert_eq!(fizzbuzz(5), "buzz".to_string())
}

#[test]
fn fifteen_returns_fizzbuzz() {
    assert_eq!(fizzbuzz(15), "fizzbuzz".to_string())
}
